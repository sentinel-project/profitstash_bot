package cmd

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/BurntSushi/toml"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

// Init return a bot conf object
func Init(conffile *string) *Conf {
	var err error
	var dat []byte
	var ownerid int64
	var botAccount = Conf{
		ConfFile: conffile,
	}
	if conffile != nil {
		if dat, err = ioutil.ReadFile(*conffile); err != nil {
			log.Fatal(err)
		}
		if _, err := toml.Decode(string(dat), &botAccount); err != nil {
			log.Fatal(err)
		}
	} else {
		botAccount.Bot.Name = os.Getenv("BOT_NETWORK")
		botAccount.Bot.Pub = os.Getenv("BOT_NAME")
		botAccount.Bot.Priv = os.Getenv("BOT_TOKEN")
		if ownerid, err = strconv.ParseInt(os.Getenv("BOT_OWNERID"), 10, 64); err != nil {
			log.Fatal(err)
		}
		botAccount.Bot.UserchatID = ownerid
	}
	return &botAccount
}

// NewClient setup a new telegram bot client
func NewClient(conf *Conf) *Client {
	port := os.Getenv("PORT")
	client := &Client{
		Conf: conf,
	}
	if os.Getenv("STAGE") == "" || os.Getenv("STAGE") == "dev" {
		return client
	}
	bot, err := tgbotapi.NewBotAPI(conf.Bot.Priv)
	if err != nil {
		log.Fatal(err)
	}
	if DeployURL == "" {
		DeployURL = conf.Bot.URL
	}
	_, err = bot.SetWebhook(tgbotapi.NewWebhook(DeployURL + bot.Token))
	if err != nil {
		log.Fatal(err)
	}
	info, err := bot.GetWebhookInfo()
	if err != nil {
		log.Fatal(err)
	}
	if info.LastErrorDate != 0 {
		log.Printf("Telegram callback failed: %s", info.LastErrorMessage)
	}
	go http.ListenAndServe(":"+port, nil)
	updateConfig := tgbotapi.NewUpdate(0)
	updateConfig.Timeout = 120
	client.Bot = bot
	client.UpdateConfig = updateConfig
	return client
}
