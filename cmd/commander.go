package cmd

import (
	"balance_api/pkg/binance"
	"balance_api/pkg/celsius"
	"balance_api/pkg/config"
	"balance_api/pkg/etherscan"
	"balance_api/pkg/n26"
	"bytes"
	"os"

	"github.com/BillotP/coinbase"
)

var (
	wholeSum  float64
	ignoreCur = "BTC"
)

const (
	// CommandTrades is the stdin or chat command to list all trades
	CommandTrades = "trades"
	// CommandAccounts is the stdin or chat command to list all accounts
	CommandAccounts = "accounts"
)

func upOrDown(val float64) string {
	switch {
	case val > 0:
		return "🟢"
	case val < 0:
		return "🔴"
	}
	return "👻"
}

// New init cmd apis
func New(client *Client, fast bool) *Commander {
	var ncmd Commander
	var exchangeAccount *config.Accounts

	if client != nil && client.Conf != nil && client.Conf.ConfFile != nil {
		exchangeAccount = config.FromFile(*client.Conf.ConfFile)
		// Load clients from toml config data
		ncmd.BinanceClient = binance.New(&exchangeAccount.Account[0].Pub, &exchangeAccount.Account[0].Priv)
		ncmd.CoinbaseClient = coinbase.New(&exchangeAccount.Account[1].Pub, &exchangeAccount.Account[1].Priv)
		ncmd.EtherscanClient = etherscan.New(&exchangeAccount.Account[2].Priv)
		ncmd.CelsiusClient = celsius.New(&exchangeAccount.Account[5].Pub, &exchangeAccount.Account[5].Priv)
		ncmd.EtherAddress = exchangeAccount.Account[6].Priv
		if !fast {
			defclientUUID := "783b2697-a1ae-4641-8955-ea8a0f499737"
			ncmd.N26Client = n26.New(&exchangeAccount.Account[3].Pub, &exchangeAccount.Account[3].Priv, &defclientUUID)
		}
		// ncmd.CelsiusReport, _ = celsius.NewCSV(CelsiusReportFname)
		ncmd.Stage = "dev"
	} else {
		// Let env var do the job
		ncmd.BinanceClient = binance.New(nil, nil)
		ncmd.CoinbaseClient = coinbase.New(nil, nil)
		ncmd.EtherscanClient = etherscan.New(nil)
		ncmd.CelsiusClient = celsius.New(nil, nil)
		ncmd.EtherAddress = os.Getenv("ETHERADDRESS")
		if !fast {
			ncmd.N26Client = n26.New(nil, nil, nil)
		}
		ncmd.Stage = os.Getenv("STAGE")
	}
	if client != nil && client.Bot != nil {
		ncmd.Bot = client.Bot
		ncmd.ReplyToID = client.Conf.Bot.UserchatID
	}
	return &ncmd
}

// AllAcounts return bank and crypto accounts
func (c *Commander) AllAcounts() (infos Platforms, err error) {
	var cryptoPlatform, bankPlatform Platforms
	var allchrt *bytes.Buffer

	if cryptoPlatform, err = c.AllCryptoAccounts(); err != nil {
		return nil, err
	}
	infos = append(infos, cryptoPlatform...)
	if bankPlatform, _ = c.AllBankAccounts(); err != nil {
		return nil, err
	}
	infos = append(infos, bankPlatform...)
	if allchrt, err = infos.ToChart(nil); err != nil {
		return nil, err
	}
	c.LogChart(*allchrt)
	return infos, err
}

// AllBankAccounts ...
func (c *Commander) AllBankAccounts() (infos Platforms, err error) {
	var pr *Platform
	var allBankChart *bytes.Buffer
	if pr, err = c.GetN26Account(); err != nil {
		return nil, err
	}
	infos = append(infos, pr)
	if allBankChart, err = infos.ToChart(nil); err != nil {
		return nil, err
	}
	c.LogChart(*allBankChart)
	return infos, err
}

// AllTrades ...
func (c *Commander) AllTrades() {
	c.PrintCoinbaseTrades()
	c.PrintBinanceTrades()
}

// Help print help
func (c *Commander) Help() {
	c.Log(`
				## 🔎 %s
				
				Commands:

					- /accounts : Print all accounts
					- /cryptoaccounts : Only crypto
					- /trades : Print all trades
					- /sell : [ASSET] [PLATFORM] [QUOTE] [HOPE]
						Sell the whole asset on platform at marketprice+hope price
					- /help : Print this help
				
				`, "STA💲H - B0T")
}
