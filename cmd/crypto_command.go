package cmd

import (
	"balance_api/pkg/binance"
	"balance_api/pkg/celsius"
	"balance_api/pkg/prices"
	"bytes"
	"fmt"
	"log"
	"strconv"

	"github.com/BillotP/coinbase/lib/models"
)

// AllCryptoAccounts ...
func (c *Commander) AllCryptoAccounts() (infos Platforms, err error) {
	var output = "⚖️       TOTAL       ⚖️\n\n"
	var cbplatform, bnplatform, etherplatform, celaccount *Platform
	var coinchrt, bichrt, celchrt, platformChrt *bytes.Buffer
	wholeSum = 0

	if cbplatform, err = c.GetCoinbaseAccount(&ignoreCur, false); err != nil {
		return nil, err
	}
	infos = append(infos, cbplatform)
	if coinchrt, err = cbplatform.ToChart(nil); err != nil {
		return nil, err
	}
	c.LogChart(*coinchrt)
	if bnplatform, err = c.GetBinanceAccount(false); err != nil {
		return nil, err
	}
	if bichrt, err = bnplatform.ToChart(nil); err != nil {
		return nil, err
	}
	c.LogChart(*bichrt)
	infos = append(infos, bnplatform)
	if etherplatform, err = c.GetEtherAccounts(); err != nil {
		return nil, err
	}
	infos = append(infos, etherplatform)
	if celaccount, err = c.GetCelsiusAccounts(); err != nil {
		return nil, err
	}
	infos = append(infos, celaccount)
	if celchrt, err = celaccount.ToChart(nil); err != nil {
		return nil, err
	}
	c.LogChart(*celchrt)
	for _, foo := range infos {
		if foo != nil {
			output += fmt.Sprintf("%s : %0.4f %s (%0.2f EUR)\n", foo.Name, foo.NativeCurrencySum, foo.NativeCurrencyAsset, foo.EURSum)
		}
	}
	output += fmt.Sprintf("\n🎈       %0.2f EUR\n", wholeSum)
	if platformChrt, err = infos.ToChart(nil); err != nil {
		return nil, err
	}
	c.Log("%s\n", output)
	c.LogChart(*platformChrt)
	c.Log("##############")
	return infos, err
}

// GetCoinbaseAccount print coinbase accounts
func (c *Commander) GetCoinbaseAccount(ignoreCur *string, showDust bool) (infos *Platform, err error) {
	var sum float64
	var output = "[Coinbase] :\n"
	var myaccounts *models.Accounts

	// Get all your accounts
	if myaccounts, err = c.CoinbaseClient.GetAccounts(); err != nil {
		c.Log("Error(CoinbaseAccount): %s\n", err.Error())
		return infos, err
	}
	myaccounts.FilterEmpty()
	infos = &Platform{
		Name:                "Coinbase",
		NativeCurrencyAsset: "BTC",
		Balances:            make([]Balance, len(myaccounts.Datas)),
	}
	// List them
	for _, account := range myaccounts.Datas {
		var nativVal, val float64
		if nativVal, err = strconv.ParseFloat(account.NativeBalance.Amount, 64); err != nil {
			log.Fatal(err)
		}
		if val, err = strconv.ParseFloat(account.Balance.Amount, 64); err != nil {
			log.Fatal(err)
		}
		if ignoreCur != nil && *ignoreCur == account.Balance.Currency ||
			showDust == false && nativVal < 0.99 {
			continue
		}
		pr, _ := c.BinanceClient.Price(account.Balance.Currency, "BTC")
		output += fmt.Sprintf("\t%s: %s (%s %s)\n",
			account.Balance.Currency,
			account.Balance.Amount,
			account.NativeBalance.Amount,
			account.NativeBalance.Currency,
		)
		infos.Balances = append(infos.Balances, Balance{
			Asset:    account.Balance.Currency,
			Value:    val,
			BTCValue: val * pr.Price,
		})
		sum += nativVal
	}
	pr, _ := c.CoinbaseClient.GetSpotPrice("BTC", "EUR")
	prval, _ := strconv.ParseFloat(pr.Data.Amount, 64)
	wholeSum += sum
	output += fmt.Sprintf("\nSum : %0.2f EUR (%0.4f BTC)\n\n", sum, sum/prval)
	infos.NativeCurrencySum = sum / prval
	infos.EURSum = sum
	c.Log("%s", output)
	return infos, err
}

// PrintCoinbaseTrades print coinbase trades
func (c *Commander) PrintCoinbaseTrades() (infos []TradeReport, err error) {
	var boughtSum float64
	var output = "[Coinbase] :\n"
	var cmp float64

	var spotprice *models.SpotPrice
	var myaccounts *models.Accounts
	var mytransac *models.Transactions

	// Get all your accounts
	if myaccounts, err = c.CoinbaseClient.GetAccounts(); err != nil {
		c.Log("Error(CoinbaseTrade): %s\n", err.Error())
		return
	}
	myaccounts.FilterEmpty()
	for _, acc := range myaccounts.Datas {
		cmp = 0
		boughtSum = 0
		if mytransac, err = c.CoinbaseClient.GetTransactionsByAccountID(acc.ID); err != nil {
			c.Log("Error(CoinbaseTrade): %s\n", err.Error())
			return
		}
		for _, tr := range mytransac.Datas {
			switch tr.Type {
			case "buy":
				price, _ := strconv.ParseFloat(tr.NativeAmount.Amount, 64)
				amount, _ := strconv.ParseFloat(tr.Amount.Amount, 64)
				boughtSum += amount
				cmp += price
			default:
				continue
			}
		}
		if boughtSum > 0 {
			avgPrice := cmp / boughtSum
			output += fmt.Sprintf("Trades for %s : bought %v at avg %0.5f price\n", acc.Balance.Currency, boughtSum, avgPrice)
			spotprice, err = c.CoinbaseClient.GetSpotPrice(acc.Balance.Currency, "EUR")
			spot, _ := strconv.ParseFloat(spotprice.Data.Amount, 64)
			evo := ((spot - avgPrice) / avgPrice) * 100
			output += fmt.Sprintf("Last price for symbol %s-%s : %v\n", acc.Balance.Currency, "EUR", spotprice.Data.Amount)
			output += fmt.Sprintf("Evolution : %.2f%% %s\n\n", evo, upOrDown(evo))
			infos = append(infos, TradeReport{
				Base:      acc.Balance.Currency,
				Quote:     "EUR",
				Qtt:       boughtSum,
				Price:     avgPrice,
				SpotPrice: spot,
				Evolution: evo,
			})
		}
	}
	c.Log("%s", output)
	return infos, err
}

// GetBinanceAccount ...
func (c *Commander) GetBinanceAccount(showDust bool) (infos *Platform, err error) {
	var sum float64
	var output = "[Binance] :\n"
	acc, err := c.BinanceClient.Account()
	if err != nil {
		c.Log("Error(BinanceAccount): %s\n", err.Error())
		return
	}
	acc.FilterEmpty()
	infos = &Platform{
		Name:                "Binance",
		NativeCurrencyAsset: "BTC",
		Balances:            make([]Balance, len(acc.Balances)),
	}
	// List them
	for _, account := range acc.Balances {
		var pr *binance.Price
		all := account.Free + account.Locked
		pr, err = c.BinanceClient.Price(account.Asset, "BTC")
		if err != nil {
			c.Log("Error(BinanceAccount): %s\n", err.Error())
			return
		}
		value := pr.Price * all
		if showDust == false && value < 0.00001 {
			continue
		}
		output += fmt.Sprintf("\t%s: %v (%0.4f BTC)\n",
			account.Asset,
			all,
			value,
		)
		infos.Balances = append(infos.Balances, Balance{
			Asset:    account.Asset,
			Value:    all,
			BTCValue: value,
		})
		sum += value
	}
	pr, _ := c.CoinbaseClient.GetSpotPrice("BTC", "EUR")
	prval, _ := strconv.ParseFloat(pr.Data.Amount, 64)
	wholeSum += sum * prval
	output += fmt.Sprintf("Sum : %0.4f BTC (%0.2f EUR)\n\n", sum, sum*prval)
	infos.NativeCurrencySum = sum
	infos.EURSum = sum * prval
	c.Log("%s", output)
	return infos, err
}

// PrintBinanceTrades ...
func (c *Commander) PrintBinanceTrades() (infos []TradeReport, err error) {
	var boughtSum, cmp float64
	var output = "[Binance] :\n"
	pairs := []Pair{
		{
			Base:  "MATIC",
			Quote: "BTC",
		},
		// {
		// 	Base:  "WABI",
		// 	Quote: "BNB",
		// },
		{
			Base:  "LINK",
			Quote: "BTC",
		},
	}

	for _, p := range pairs {
		boughtSum = 0
		cmp = 0
		var trds []binance.Trade
		var spotprice *binance.Price
		trds, err = c.BinanceClient.Trades(p.Base, p.Quote)
		if err != nil {
			c.Log("Error(BinanceTrade): %s\n", err.Error())
			return
		}
		for i := range trds {
			if trds[i].IsBuyer {
				boughtSum += trds[i].Quantity
				cmp += trds[i].Quantity * trds[i].Price
			}
		}
		avgPrice := cmp / boughtSum
		output += fmt.Sprintf("Trades for %s : bought %v at avg %0.5f price\n", p.Base, boughtSum, avgPrice)
		spotprice, err = c.BinanceClient.Price(p.Base, p.Quote)
		if err != nil {
			c.Log("Error(BinanceTrades): %s\n", err.Error())
			return
		}
		evo := ((spotprice.Price - avgPrice) / avgPrice) * 100
		output += fmt.Sprintf("Last price for symbol %s : %+v\n", p.Base+p.Quote, spotprice.Price)
		output += fmt.Sprintf("Evolution : %.2f%% %s\n\n", evo, upOrDown(evo))
		infos = append(infos, TradeReport{
			Base:      p.Base,
			Quote:     p.Quote,
			Qtt:       boughtSum,
			Price:     avgPrice,
			SpotPrice: spotprice.Price,
			Evolution: evo,
		})
	}
	c.Log("%s", output)
	return infos, nil
}

// GetEtherAccounts print ...
func (c *Commander) GetEtherAccounts() (infos *Platform, err error) {
	var output = "[Ethereum] :\n"
	ethAmount, err := c.EtherscanClient.GetAddressBalance(c.EtherAddress)
	if err != nil {
		c.Log("Error(GetEtherAccounts): %s\n", err.Error())
		return
	}
	ethSpotPrice, _ := c.CoinbaseClient.GetSpotPrice("ETH", "EUR")
	ethSpotPriceValue, _ := strconv.ParseFloat(ethSpotPrice.Data.Amount, 64)
	ethBtcSpotPrice, _ := c.BinanceClient.Price("ETH", "BTC")
	ethEurValue := ethAmount * ethSpotPriceValue
	ethBtcValue := ethAmount * ethBtcSpotPrice.Price
	output += fmt.Sprintf("\tETH: %0.4f (%0.4f BTC / %0.3f EUR)\n", ethAmount, ethBtcValue, ethEurValue)
	wholeSum += ethEurValue
	tokenAmount, _ := c.EtherscanClient.GetAddressTokens(c.EtherAddress)
	tokenSpotPrice, _ := c.BinanceClient.Price("RLC", "ETH")
	tknsEthValue := tokenAmount * tokenSpotPrice.Price
	tknsEurValue := tknsEthValue * ethSpotPriceValue
	tknBtcValue := tknsEthValue * ethBtcSpotPrice.Price
	output += fmt.Sprintf("\tRLC: %0.4f (%0.3f ETH / %0.3f EUR)\n", tokenAmount, tknsEthValue, tknsEurValue)
	wholeSum += tknsEurValue
	output += fmt.Sprintf("\nSum : %0.f EUR\n\n", tknsEurValue+ethEurValue)
	c.Log("%s", output)
	return &Platform{
		Name:                "ETH Wallet",
		NativeCurrencyAsset: "BTC",
		NativeCurrencySum:   (tknsEthValue + ethAmount) * ethBtcSpotPrice.Price,
		EURSum:              tknsEurValue + ethEurValue,
		Balances: []Balance{
			{
				Asset:    "ETH",
				Value:    ethAmount,
				BTCValue: ethBtcValue,
			},
			{
				Asset:    "RLC",
				Value:    tokenAmount,
				BTCValue: tknBtcValue,
			},
		},
	}, err
}

// GetCelsiusAccounts ...
func (c *Commander) GetCelsiusAccounts() (infos *Platform, err error) {
	var BTCSum float64
	var output = "[Celsius] :\n"
	var celsiusBalances *celsius.Balances
	var prInfo = prices.New(nil, nil, nil)

	if celsiusBalances, err = c.CelsiusClient.GetBalances(); err != nil {
		return nil, err
	}
	celsiusBalances.GetBTCVal(prInfo)
	celsiusBalances.FilterEmpty()
	infos = &Platform{
		Name:                "Celsius",
		NativeCurrencyAsset: "BTC",
		Balances:            make([]Balance, len(celsiusBalances.Datas)),
	}
	pr, _ := c.CoinbaseClient.GetSpotPrice("BTC", "EUR")
	prval, _ := strconv.ParseFloat(pr.Data.Amount, 64)
	for _, bal := range celsiusBalances.Datas {
		output += fmt.Sprintf("\t%s: %0.4f (%0.4f BTC / %0.2f EUR)\n", bal.Asset, bal.Value, *bal.BTCValue, *bal.BTCValue*prval)
		BTCSum += *bal.BTCValue
		infos.Balances = append(infos.Balances, Balance{
			Asset:    bal.Asset,
			Value:    bal.Value,
			BTCValue: *bal.BTCValue,
		})
	}
	EURSum := BTCSum * prval
	output += fmt.Sprintf("\nSum : %0.2f BTC (%0.2f EUR)\n\n", BTCSum, EURSum)
	infos.NativeCurrencySum = BTCSum
	infos.EURSum = EURSum
	wholeSum += EURSum
	c.Log("%s", output)
	return infos, err
}

// SellAll coin by asset
func (c *Commander) SellAll(asset, platform, quote string, hope float64) {
	// var sum float64
	// var output = "[Binance] :\n"
	switch platform {
	case "binance":
		{
			var boughtSum, cmp float64
			acc, err := c.BinanceClient.Account()
			if err != nil {
				c.Log("Error(BinanceAccount): %s\n", err.Error())
				return
			}
			el := acc.Get(asset)
			if el == nil {
				c.Log("Error(BinanceAccount): %s: no such asset\n", asset)
				return
			}
			trades, err := c.BinanceClient.Trades(asset, quote)
			if err != nil {
				c.Log("Error(BinanceTrades): %s\n", err.Error())
				return
			}
			currprice, err := c.BinanceClient.Price(asset, quote)
			if err != nil {
				c.Log("Error(BinancePrice): %s\n", err.Error())
				return
			}
			for i := range trades {
				if trades[i].IsBuyer {
					boughtSum += trades[i].Quantity
					cmp += trades[i].Quantity * trades[i].Price
				}
			}
			avgPrice := cmp / boughtSum
			c.Log("Have bought %v at price %v\n", boughtSum, avgPrice)
			// sellPrice := currprice.Price + (hope / 100 * currprice.Price)
			evo := ((currprice.Price - avgPrice) / avgPrice) * 100
			c.Log("Will try to sell %v at %v price (%.2f%%)\n", el.Free, currprice.Price, evo)

			var res *string
			if res, err = c.BinanceClient.NewOrder(binance.NOrder{
				Symbol:   asset + quote,
				Side:     binance.OrderSideSell,
				Type:     binance.Market,
				Quantity: el.Free,
				// Price:    sellPrice,
				// TimeInForce: binance.TimeInforceGoodTillCanceled,
			}); err != nil {
				c.Log("Error(BinanceNOrder): %s\n", err.Error())
			}
			if res != nil {
				c.Log("Got resp %s\n", *res)
			}
			// c.Bot.
			return
		}
	default:
		c.Log("%s : no such platform\n", platform)
		return
	}
}
