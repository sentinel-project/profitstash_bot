package cmd

import (
	"log"
	"strconv"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

// BotInfo is the config for a bot
type BotInfo struct {
	Name       string
	Pub        string
	Priv       string
	URL        string
	UserchatID int64
}

// Conf ...
type Conf struct {
	Bot      BotInfo
	ConfFile *string
}

// Client is the telegram bot client wrapper struct
type Client struct {
	Conf         *Conf
	Bot          *tgbotapi.BotAPI
	UpdateConfig tgbotapi.UpdateConfig
}

// RunBot run telegram bot
func (cli *Client) RunBot() {
	botUdpates := cli.Bot.ListenForWebhook("/" + cli.Bot.Token)
	for update := range botUdpates {
		if update.Message == nil {
			continue
		}
		// Checking if user is authorized to talk to this bot
		if update.Message.Chat.ID != cli.Conf.Bot.UserchatID {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "forbidden dude ...")
			msg.ReplyToMessageID = update.Message.MessageID
			if _, err := cli.Bot.Send(msg); err != nil {
				log.Fatal(err)
			}
			continue
		} else {
			tmstp := time.Now().Local().Format(time.RFC3339)
			recvCmd := update.Message.Command()
			switch recvCmd {
			case "cryptoaccounts":
				cmder := New(cli, true)
				cmder.Log("### 🤖 CryptoAccounts\n[%s]\n", tmstp)
				if _, err := cmder.AllCryptoAccounts(); err != nil {
					cmder.Log(err.Error())
				}
				break
			case "accounts":
				cmder := New(cli, false)
				cmder.Log("### 🏦 AllAccounts\n[%s]\n", tmstp)
				if _, err := cmder.AllAcounts(); err != nil {
					cmder.Log(err.Error())
				}
				break
			case "trades":
				cmder := New(cli, true)
				cmder.Log("### 📊 Trades\n[%s]\n", tmstp)
				cmder.AllTrades()
				break
			case "sell":
				cmder := New(cli, true)
				cmder.Log("### Sell\n[%s]\n", tmstp)
				args := strings.Split(update.Message.Text, " ")
				hope, _ := strconv.ParseFloat(args[4], 64)
				cmder.SellAll(args[1], args[2], args[3], hope)
				break
			case "help":
				cmder := New(cli, true)
				cmder.Help()
				break
			default:
				cmder := New(cli, true)
				cmder.Log("[%s]: No such command dude, use /help to see what's available\n", recvCmd)
			}
		}
	}
}
