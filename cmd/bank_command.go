package cmd

import (
	"balance_api/pkg/n26"
	"fmt"
)

// GetN26Account ...
func (c *Commander) GetN26Account() (infos *Platform, err error) {
	var n26Account *n26.Accounts
	var output = "[N26] :\n"
	if n26Account, err = c.N26Client.Accounts(); err != nil {
		c.Log("Error(N26Account): %s\n", err.Error())
		return
	}
	output += fmt.Sprintf("\t%s: %0.3f\n", n26Account.Currency, n26Account.AvailableBalance)
	output += fmt.Sprintf("Sum : %0.2f EUR\n\n", n26Account.AvailableBalance)
	wholeSum += n26Account.AvailableBalance
	c.Log("%s", output)
	return &Platform{
		Name:                "N26",
		NativeCurrencyAsset: n26Account.Currency,
		NativeCurrencySum:   n26Account.AvailableBalance,
		EURSum:              n26Account.AvailableBalance,
		Balances: []Balance{
			{
				Asset: n26Account.Currency,
				Value: n26Account.AvailableBalance,
			},
		},
	}, err
}
