package cmd

import (
	"log"
	"os"
	"testing"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var gCmmder *Commander

func TestMain(m *testing.M) {
	testconfile := "../conf.toml"
	mockClient := Client{
		Conf: &Conf{
			Bot: BotInfo{
				Name:       "test",
				UserchatID: 123,
			},
			ConfFile: &testconfile,
		},
	}
	gCmmder = New(&mockClient, true)
	if gCmmder == nil {
		log.Fatal("gCmmder is nil")
	}
	os.Exit(m.Run())
}

func TestNew(t *testing.T) {
	testconfile := "../conf.toml"
	mockClient := Client{
		Conf: &Conf{
			Bot: BotInfo{
				Name:       "test",
				UserchatID: 123,
			},
			ConfFile: &testconfile,
		},
	}
	cmder := New(&mockClient, true)
	if cmder == nil {
		t.Error("want cmder got nil")
	}
	cmder = New(nil, true)
	if cmder == nil {
		t.Error("want cmder got nil")
	}
	mockClient.Bot = new(tgbotapi.BotAPI)
	mockClient.Conf.Bot.UserchatID = 123
	cmder = New(&mockClient, true)
}

func TestAllAccounts(t *testing.T) {
	if _, err := gCmmder.AllAcounts(); err != nil {
		t.Errorf("Error %s\n", err.Error())
	}
}

func ExampleCommander_LogLists() {
	ncmd := Commander{}
	list := []interface{}{TradeReport{
		Base:      "WABI",
		Quote:     "BTC",
		Qtt:       4088,
		Price:     0.0067,
		SpotPrice: 0.0082,
		Evolution: 17.87,
	}, TradeReport{
		Base:      "LINK",
		Quote:     "BNB",
		Qtt:       30,
		Price:     0.067,
		SpotPrice: 0.082,
		Evolution: 12.87,
	}}
	ncmd.LogLists(list)
	// Output:
	// +------+-------+------+--------+-----------+-----------+
	// | BASE | QUOTE | QTT  | PRICE  | SPOTPRICE | EVOLUTION |
	// +------+-------+------+--------+-----------+-----------+
	// | WABI | BTC   | 4088 | 0.0067 | 0.0082    | 17.87     |
	// | LINK | BNB   | 30   | 0.067  | 0.082     | 12.87     |
	// +------+-------+------+--------+-----------+-----------+
}

func TestSymbol(t *testing.T) {
	var err error
	var res string
	var sep = "-"
	var p = Pair{
		Base:  "A",
		Quote: "B",
	}
	var bad = Pair{
		Base:  "AAAAAAAAAAAAAAA",
		Quote: "BBBBBBBBBBBBBBB",
	}
	res, err = p.Symbol(nil)
	if res != "AB" {
		t.Errorf("invalid symbol want AB got %s", res)
	}
	res, err = p.Symbol(&sep)
	if res != "A-B" {
		t.Errorf("invalid symbol want A-B got %s", res)
	}
	if _, err = bad.Symbol(nil); err == nil {
		t.Errorf("bad symbol want error got nil")
	}
}
