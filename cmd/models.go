package cmd

import (
	"balance_api/pkg/binance"
	"balance_api/pkg/celsius"
	"balance_api/pkg/etherscan"
	"balance_api/pkg/n26"
	"bytes"
	"fmt"
	"os"
	"strings"

	"github.com/BillotP/coinbase"
	"github.com/wcharczuk/go-chart"
	"github.com/wcharczuk/go-chart/drawing"
)

var (
	// DeployURL is the heroku URL this bot is listening on
	DeployURL = os.Getenv("URL")
	// MaxSymbolLength is the max length for a ticker
	MaxSymbolLength = 5
	// ErrTooLongSymbol is returned when a symbol couldn't be valid for a trading symbol
	ErrTooLongSymbol = fmt.Errorf("symbol is too long, use short version ticker like 'BTC', 'LTC'")
)

// Commander ...
type Commander struct {
	Client
	BinanceClient   *binance.Client
	CoinbaseClient  *coinbase.Client
	EtherscanClient *etherscan.Client
	CelsiusClient   *celsius.Client
	CelsiusReport   *celsius.Report
	N26Client       *n26.Client
	EtherAddress    string
	ReplyToID       int64
	Stage           string
}

// TradeReport ...
type TradeReport struct {
	Base      string
	Quote     string
	Qtt       float64
	Price     float64
	SpotPrice float64
	Evolution float64
}

// Trade ...
type Trade struct {
	Pair     Pair
	Side     string
	Price    float64
	Quantity float64
}

// Balance ...
type Balance struct {
	Asset    string
	Value    float64
	BTCValue float64
}

// Platform ...
type Platform struct {
	Name                string
	NativeCurrencyAsset string
	NativeCurrencySum   float64
	EURSum              float64
	Balances            []Balance
}

// Platforms is an array of platform
type Platforms []*Platform

// ToChart get pie charts for platform distribution datas
func (p Platforms) ToChart(savepath *string) (*bytes.Buffer, error) {
	var eursum float64
	pie := chart.PieChart{
		Width:        512,
		Height:       512,
		ColorPalette: chart.AlternateColorPalette,
		TitleStyle: chart.Style{
			Show:                true,
			Padding:             chart.NewBox(90, 0, 30, 30),
			FontColor:           chart.ColorWhite,
			TextWrap:            chart.TextWrapWord,
			TextVerticalAlign:   chart.TextVerticalAlignMiddleBaseline,
			TextHorizontalAlign: chart.TextHorizontalAlignLeft,
		},
		Background: chart.Style{
			FillColor: drawing.ColorFromHex("1B3A44"),
			Padding:   chart.NewBox(50, 30, 30, 0),
		},
		Canvas: chart.Style{
			FillColor: drawing.ColorFromHex("1B3A44"),
		},
	}
	for _, p := range p {
		if p != nil {
			eursum += p.EURSum
			pie.Values = append(pie.Values, chart.Value{
				Label: fmt.Sprintf("%s %0.4f EUR", p.Name, p.EURSum),
				Value: p.EURSum,
				Style: chart.Style{
					FontSize: 9,
				},
			})
		}
	}
	pie.Title = fmt.Sprintf("%0.2f EUR", eursum)
	if savepath != nil {
		f, _ := os.Create(*savepath)
		defer f.Close()
		pie.Render(chart.PNG, f)
	}
	buffer := &bytes.Buffer{}
	err := pie.Render(chart.PNG, buffer)
	return buffer, err
}

// ToChart get a pie chart for a Platform.Balances object
func (p *Platform) ToChart(savepath *string) (*bytes.Buffer, error) {
	pie := chart.PieChart{
		Width:        512,
		Height:       512,
		ColorPalette: chart.AlternateColorPalette,
		Title:        fmt.Sprintf("%s\n%0.2f EUR", p.Name, p.EURSum),
		TitleStyle: chart.Style{
			Show:                true,
			Padding:             chart.NewBox(90, 0, 30, 30),
			FontColor:           chart.ColorWhite,
			TextWrap:            chart.TextWrapWord,
			TextVerticalAlign:   chart.TextVerticalAlignMiddleBaseline,
			TextHorizontalAlign: chart.TextHorizontalAlignLeft,
		},
		Background: chart.Style{
			FillColor: drawing.ColorFromHex("1B3A44"),
			Padding:   chart.NewBox(50, 30, 30, 0),
		},
		Canvas: chart.Style{
			FillColor: drawing.ColorFromHex("1B3A44"),
		},
	}
	for _, bal := range p.Balances {
		pie.Values = append(pie.Values, chart.Value{
			Label: fmt.Sprintf("%s %0.4f BTC", bal.Asset, bal.BTCValue),
			Value: bal.BTCValue,
			Style: chart.Style{
				FontSize: 9,
				// FontColor: ,
			},
		})
	}
	if savepath != nil {
		f, _ := os.Create(*savepath)
		defer f.Close()
		pie.Render(chart.PNG, f)
	}
	buffer := &bytes.Buffer{}
	err := pie.Render(chart.PNG, buffer)
	return buffer, err
}

// Pair is a (crypto)currency exchange pair
type Pair struct {
	Base  string
	Quote string
}

// Symbol return market symbol for a pair
func (p Pair) Symbol(sep *string) (string, error) {
	var pair string
	if len(p.Base) > MaxSymbolLength ||
		len(p.Quote) > MaxSymbolLength {
		return pair, ErrTooLongSymbol
	}
	p.Base = strings.ToUpper(p.Base)
	p.Quote = strings.ToUpper(p.Quote)
	if sep != nil {
		pair = p.Base + *sep + p.Quote
	} else {
		pair = p.Base + p.Quote
	}
	return pair, nil
}
