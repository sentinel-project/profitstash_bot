package cmd

import (
	"bytes"
	"fmt"
	"log"
	"reflect"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/jedib0t/go-pretty/table"
)

// Log dump to stdout and bot channel if any
func (c *Commander) Log(format string, args ...interface{}) {
	out := fmt.Sprintf(format, args...)
	if c.Stage == "dev" {
		fmt.Print(out)
	}
	if c.Bot != nil {
		msg := tgbotapi.NewMessage(c.ReplyToID, out)
		msg.ParseMode = "Markdown"
		if _, err := c.Bot.Send(msg); err != nil {
			log.Fatal(err)
		}
	}
}

// LogChart send the balance chart png to telegram
func (c *Commander) LogChart(chart bytes.Buffer) {
	image := tgbotapi.FileBytes{Name: "chart.png", Bytes: chart.Bytes()}
	if c.Stage == "dev" {
		return
	}
	if _, err := c.Bot.Send(tgbotapi.NewPhotoUpload(c.ReplyToID, image)); err != nil {
		log.Fatal(err)
	}
}

func getHeaders(obj interface{}) table.Row {
	var nrow table.Row
	v := reflect.ValueOf(obj)
	typeOfS := v.Type()
	for i := 1; i < v.NumField(); i++ {
		nrow = append(nrow, typeOfS.Field(i).Name)
	}
	return nrow
}

func getRows(objs []interface{}) []table.Row {
	var rows []table.Row
	for i := range objs {
		var nrow table.Row
		v := reflect.ValueOf(objs[i])
		for i := 1; i < v.NumField(); i++ {
			nrow = append(nrow, fmt.Sprintf("%v", v.Field(i)))
		}
		rows = append(rows, nrow)
	}
	return rows
}

// LogLists dump slices to stdout with go-pretty
func (c *Commander) LogLists(list []interface{}) {
	// simple table with zero customizations
	tw := table.NewWriter()
	// append a header row
	headerRow := getHeaders(list[0])
	tw.AppendHeader(headerRow)
	// append some data rows
	tableRows := getRows(list)
	tw.AppendRows(tableRows)
	c.Log("%s", tw.Render())
}
