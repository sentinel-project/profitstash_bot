package main

import (
	"balance_api/cmd"
	"os"
)

func main() {
	var cinf *string
	var defConf = "conf.toml"
	if os.Getenv("STAGE") == "" {
		cinf = &defConf
	}
	botConf := cmd.Init(cinf)
	ncli := cmd.NewClient(botConf)
	ncli.RunBot()
}
