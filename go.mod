module balance_api

go 1.14

require (
	github.com/BillotP/coinbase v0.4.3
	github.com/BurntSushi/toml v0.3.1
	github.com/blend/go-sdk v1.1.1 // indirect
	github.com/go-openapi/strfmt v0.19.5 // indirect
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/google/go-cmp v0.5.1 // indirect
	github.com/google/go-querystring v1.0.0
	github.com/google/uuid v1.1.1
	github.com/jedib0t/go-pretty v4.3.0+incompatible
	github.com/jinzhu/gorm v1.9.15
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/wcharczuk/go-chart v2.0.1+incompatible
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de // indirect
	golang.org/x/image v0.0.0-20200801110659-972c09e46d76 // indirect
)
