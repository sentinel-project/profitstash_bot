package models

import (
	"database/sql/driver"

	"github.com/jinzhu/gorm"
)

// TransactionType is an asset flow
type TransactionType string

const (
	// Deposit is a deposit to celsius account
	Deposit TransactionType = "deposit"
	// Withdraw is a withdraw from celsius account
	Withdraw TransactionType = "withdraw"
)

// Scan ...
func (t *TransactionType) Scan(value interface{}) error {
	*t = TransactionType(value.([]byte))
	return nil
}

// Value ...
func (t TransactionType) Value() (driver.Value, error) {
	return string(t), nil
}

// User is a user to this app
type User struct {
	gorm.Model
	Name string
	Key  string
}

// Balance is the celsius balance structure
type Balance struct {
	gorm.Model
	UserID   int    `gorm:"index:userbalance"`
	Symbol   string `gorm:"size:5;index:userbalance"`
	Quantity float64
}

// Transaction is an asset flow (withdraw or deposit)
type Transaction struct {
	gorm.Model
	UserID   int
	Type     TransactionType
	Quantity float64
	Symbol   string `gorm:"size:5"`
	From     string
	To       string
	Date     *string
}

// Migrate do the celsius tables migrations
func Migrate(db *gorm.DB) {
	// Migrate the schema
	db.AutoMigrate(&User{})
	db.AutoMigrate(&Balance{})
	db.AutoMigrate(&Transaction{})
}
