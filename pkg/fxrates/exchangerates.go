package fxrates

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// APIData ...
type APIData struct {
	Rates map[string]float64 `json:"rates"`
	Base  string             `json:"base"`
	Date  time.Time          `json:"date"`
}

var savedRate *APIData

// UnmarshalJSON custom cause time parsing is impossible
func (d *APIData) UnmarshalJSON(body []byte) error {
	var err error
	dateLayout := "2006-01-02"
	var tmp = struct {
		Rates map[string]float64 `json:"rates"`
		Base  string             `json:"base"`
		Date  string             `json:"date"`
	}{}
	if err = json.Unmarshal(body, &tmp); err != nil {
		return err
	}
	d.Base = tmp.Base
	d.Rates = tmp.Rates
	if d.Date, err = time.Parse(dateLayout, tmp.Date); err != nil {
		return err
	}
	return nil
}

// GetCurrentExchangeRate ...
func (d APIData) GetCurrentExchangeRate(symbol string) map[string]float64 {
	var err error
	var resp APIData
	var body []byte
	var res *http.Response
	var url = fmt.Sprintf("https://api.exchangeratesapi.io/latest?symbols=%s", symbol)

	if savedRate == nil {
		if res, err = http.Get(url); err != nil {
			fmt.Printf("Error(GetCurrentExchangeRates): %s\n", err.Error())
			return nil
		}
		if body, err = ioutil.ReadAll(res.Body); err != nil {
			fmt.Printf("Error(GetCurrentExchangeRates): %s\n", err.Error())
			return nil
		}
		defer res.Body.Close()
		if jsonErr := resp.UnmarshalJSON(body); jsonErr != nil {
			fmt.Printf("Error(GetCurrentExchangeRates): %s\n", err.Error())
			return nil
		}
		savedRate = &resp
	}
	return savedRate.Rates
}

// ToEUR ...
func (d APIData) ToEUR(value float64, symbol string) float64 {
	var resp = d.GetCurrentExchangeRate(symbol)
	return value / resp[symbol]
}
