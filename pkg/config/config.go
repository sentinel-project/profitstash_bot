package config

import (
	"io/ioutil"
	"log"

	"github.com/BurntSushi/toml"
)

// Account is an api account
type Account struct {
	Name string
	Pub  string
	Priv string
}

// Accounts is multiple api accounts
type Accounts struct {
	Account []Account
}

// FromFile load account object from specific toml file
func FromFile(fpath string) *Accounts {
	var (
		err      error
		dat      []byte
		accounts Accounts
	)

	if dat, err = ioutil.ReadFile(fpath); err != nil {
		log.Fatal(err)
	}
	if _, err := toml.Decode(string(dat), &accounts); err != nil {
		log.Fatal(err)
	}
	return &accounts
}

// AccountByName return an account by its name
func (a *Accounts) AccountByName(name string) *Account {
	for i := range a.Account {
		if a.Account[i].Name == name {
			return &a.Account[i]
		}
	}
	return nil
}
