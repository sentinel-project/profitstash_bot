package n26

import "net/http"

const (
	// ContentTypeURLEncoded is mime for url encoded params
	ContentTypeURLEncoded = "application/x-www-form-urlencoded"
	// ContentTypeJSON is mime for json params
	ContentTypeJSON = "application/json"
)

// MFAChallenge params for mfa request
type MFAChallenge struct {
	ChallengeType string `json:"challengeType"`
	MFAToken      string `json:"mfaToken"`
}

var (
	// InitToken launch an auth request (getting mfaToken)
	InitToken = Endpoint{
		Value:       "/oauth/token",
		Verb:        http.MethodPost,
		ContentType: ContentTypeURLEncoded,
		IsAuth:      false,
		Params: map[string]bool{
			"grant_type": true,
			"username":   true,
			"password":   true,
		},
	}
	// MfaChallenge engage the user to confirm the challenge on his/her mobile app
	MfaChallenge = Endpoint{
		Value:       "/api/mfa/challenge",
		Verb:        http.MethodPost,
		ContentType: ContentTypeJSON,
		IsAuth:      false,
		Params: map[string]bool{
			"challenge_type": true,
			"mfaToken":       true,
		},
	}
	// AskAuthConfirm get bearer token once user validate MfaChallenge
	AskAuthConfirm = Endpoint{
		Value:       "/oauth/token",
		Verb:        http.MethodPost,
		ContentType: ContentTypeURLEncoded,
		IsAuth:      false,
		Params: map[string]bool{
			"grant_type": true,
			"mfaToken":   true,
		},
	}
	// AccountsInfo return user account info object
	AccountsInfo = Endpoint{
		Value:       "/api/accounts",
		Verb:        http.MethodGet,
		ContentType: "",
		IsAuth:      true,
		Params:      nil,
	}
)
