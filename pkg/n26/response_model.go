package n26

// Error is an error response
type Error struct {
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
}

// InitAuth is the response for auth init process (mfa request)
type InitAuth struct {
	UserMessage struct {
		Title  string `json:"title"`
		Detail string `json:"detail"`
	} `json:"userMessage"`
	MfaToken         string `json:"mfaToken"`
	ErrorDescription string `json:"error_description"`
	Detail           string `json:"detail"`
	HostURL          string `json:"hostUrl"`
	Type             string `json:"type"`
	Error            string `json:"error"`
	Title            string `json:"title"`
	Message          string `json:"message"`
	UserID           string `json:"userId"`
	Status           int64  `json:"status"`
}

// ChallengeResp is the response for
type ChallengeResp struct {
	ChallengeType string `json:"challengeType"`
	Detail        string `json:"detail"`
	Message       string `json:"message"`
	Error         string `json:"error"`
	Status        int64  `json:"status"`
}

// AuthResponse is the token grant response
type AuthResponse struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int64  `json:"expires_in"`
	HostURL      string `json:"host_url"`
}

// Accounts is the /accounts model
type Accounts struct {
	ID               string  `json:"id"`
	PhysicalBalance  *string `json:"physicalBalance"`
	AvailableBalance float64 `json:"availableBalance"`
	UsableBalance    float64 `json:"usableBalance"`
	IBAN             string  `json:"iban"`
	BIC              string  `json:"bic"`
	BankName         string  `json:"bankName"`
	Seized           bool    `json:"seized"`
	Currency         string  `json:"currency"`
	LegalEntity      string  `json:"legalEntity"`
	Users            []struct {
		UserID   string `json:"userId"`
		UserRole string `json:"userRole"`
	} `json:"users"`
	ExternalID struct {
		IBAN string `json:"iban"`
	}
}
