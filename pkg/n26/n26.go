package n26

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/google/uuid"
)

var (
	// SavedBearer ...
	SavedBearer = os.Getenv("AUTH")
	// AuthRetry is the number of time we check for user mfa challenge
	AuthRetry = 10
	// AuthDelay is the number of second we wait between request
	AuthDelay = 5 * time.Second
)

// New return a new client against a username and password pairs
func New(username *string, password *string, deviceuuid *string) *Client {
	if N26Username == "" && username != nil {
		N26Username = *username
	}
	if N26Password == "" && password != nil {
		N26Password = *password
	}
	if N26DEVICEUUID == "" && deviceuuid != nil {
		N26DEVICEUUID = *deviceuuid
	} else if N26DEVICEUUID == "" {
		N26DEVICEUUID = uuid.New().String()
	}
	N26Account := &Authentication{
		Username:   N26Username,
		Password:   N26Password,
		BaseURL:    BaseURL,
		DeviceUUID: N26DEVICEUUID,
		Client:     *http.DefaultClient,
	}
	nCli := &Client{
		Auth: N26Account,
	}
	if SavedBearer != "" {
		nCli.Auth.AccessToken = SavedBearer
		return nCli
	}
	if err := nCli.Authenticate(); err != nil {
		fmt.Printf("Error(n26/New): %s\n", err.Error())
		return nil
	}
	return nCli
}

// Do ...
func (c Client) Do(e Endpoint, params *string, holder interface{}) error {
	var (
		err      error
		reqParam string
		req      *http.Request
		resp     *http.Response
	)
	// Creating request
	if req, err = http.NewRequest(e.Verb, c.Auth.BaseURL+e.Value, nil); err != nil {
		return err
	}
	// Setting header
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", e.ContentType)
	req.Header.Set("Device-Token", c.Auth.DeviceUUID)
	if e.IsAuth {
		req.Header.Set("Authorization", "bearer "+c.Auth.AccessToken)
	} else {
		req.Header.Set("Authorization", DefaultAuth)
	}
	// Setting request param if any
	if params != nil && e.ContentType == "application/x-www-form-urlencoded" {
		reqParam = url.QueryEscape(*params)
	}
	if e.Verb != http.MethodGet {
		req.Body = ioutil.NopCloser(strings.NewReader(*params))
	} else {
		req.URL.RawQuery = reqParam
	}
	// Doing request
	if resp, err = c.Auth.Client.Do(req); err != nil {
		return err
	}
	defer resp.Body.Close()
	// Deserialize
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	str := buf.String()
	if err := json.Unmarshal([]byte(str), &holder); err != nil {
		return err
	}
	return nil
}

// PasswordGrant is the first step of oauth process
func (c *Client) PasswordGrant() (*InitAuth, error) {
	var initResp InitAuth

	query := fmt.Sprintf("grant_type=password&username=%s&password=%s", c.Auth.Username, c.Auth.Password)
	if err := c.Do(InitToken, &query, &initResp); err != nil {
		return nil, fmt.Errorf("PasswordGrant: %s", err.Error())
	}
	if c.Auth.MfaToken = initResp.MfaToken; c.Auth.MfaToken == "" {
		var errLog = initResp.Detail
		if errLog == "" {
			errLog = initResp.Message
		}
		return nil, fmt.Errorf("PasswordGrant: %s", initResp.Detail)
	}
	return &initResp, nil
}

// ChallengeGrant ... is the 2nd step for auth process (launch user confirm process)
func (c *Client) ChallengeGrant() (*ChallengeResp, error) {
	var err error
	var query string
	var blob []byte
	var challengeResp ChallengeResp

	if blob, err = json.Marshal(&MFAChallenge{
		ChallengeType: "oob",
		MFAToken:      c.Auth.MfaToken,
	}); err != nil {
		return nil, fmt.Errorf("ChallengeGrant: %s", err.Error())
	}
	query = string(blob)
	if err := c.Do(MfaChallenge, &query, &challengeResp); err != nil {
		return nil, fmt.Errorf("ChallengeGrant: %s", err.Error())
	}
	if challengeResp.ChallengeType != "oob" {
		return nil, fmt.Errorf("ChallengeGrant: %s", challengeResp.Detail)
	}
	return &challengeResp, nil
}

// GetToken try to get a new bearer after mfa challenge
func (c *Client) GetToken() (*AuthResponse, error) {
	var tokenResp AuthResponse
	query := fmt.Sprintf("grant_type=mfa_oob&mfaToken=%s", c.Auth.MfaToken)
	for i := 0; i <= AuthRetry; i++ {
		// Loop to check if user have confirm
		if err := c.Do(AskAuthConfirm, &query, &tokenResp); err != nil {
			return nil, fmt.Errorf("GetToken: %s", err.Error())
		}
		if tokenResp.AccessToken == "" {
			time.Sleep(AuthDelay)
		} else {
			break
		}
	}
	return &tokenResp, nil
}

// RefreshGrant is needed when token is expired
func (c *Client) RefreshGrant() (*InitAuth, error) {
	var initResp InitAuth
	query := fmt.Sprintf("grant_type=refresh_token&refresh_token=%s", c.Auth.RefreshToken)
	if err := c.Do(InitToken, &query, &initResp); err != nil {
		return nil, fmt.Errorf("RefreshGrant: %s", err.Error())
	}
	if c.Auth.MfaToken = initResp.MfaToken; c.Auth.MfaToken == "" {
		return nil, fmt.Errorf("RefreshGrant: %s", initResp.Detail)
	}
	return &initResp, nil
}

// Authenticate start the process to get a bearer token (need validation on 2nd device)
func (c *Client) Authenticate() (err error) {
	var authResp *AuthResponse

	if _, err = c.PasswordGrant(); err != nil {
		return err
	}
	if _, err = c.ChallengeGrant(); err != nil {
		return err
	}
	if authResp, err = c.GetToken(); err != nil {
		return err
	}
	fmt.Printf("Got acess token : [%s]\n", authResp.AccessToken)
	c.Auth.AccessToken = authResp.AccessToken
	c.Auth.RefreshToken = authResp.RefreshToken
	return nil
}

// Accounts return all n26 accounts info
func (c *Client) Accounts() (*Accounts, error) {
	var accs Accounts
	if err := c.Do(AccountsInfo, nil, &accs); err != nil {
		return nil, fmt.Errorf("Accounts: %s", err.Error())
	}
	return &accs, nil
}
