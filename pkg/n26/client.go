package n26

import (
	"net/http"
	"os"
)

const (
	// BaseURL is the n26 api base URL
	BaseURL = "https://api.tech26.de"
	// DefaultAuth is a basic auth
	DefaultAuth = "Basic YW5kcm9pZDpzZWNyZXQ="
)

var (
	// N26Username is the default en var to store user
	N26Username = os.Getenv("N26PUB")
	// N26Password is the default en var to store user password
	N26Password = os.Getenv("N26PRIV")
	// N26DEVICEUUID is an identifier for the device this programm is running on
	N26DEVICEUUID = os.Getenv("N26BOTID")
)

// Token is the token
type Token struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	MfaToken     string `json:"mfaToken"`
}

// Authentication Struct implements the Authentication interface and takes
// care of authenticating RPC requests for clients
type Authentication struct {
	Username   string
	Password   string
	DeviceUUID string
	BaseURL    string
	Client     http.Client
	Token
}

// Endpoint represent an API endpoint
type Endpoint struct {
	Value       string
	Verb        string
	ContentType string
	IsAuth      bool
	Params      interface{}
}

// Client ...
type Client struct {
	Auth *Authentication
}
