package binance

// FilterEmpty the account object balances by asset or by removing empty balancess
func (a *Account) FilterEmpty() {
	var fltr []Balance
	var empty = float64(0)
	for i := range a.Balances {
		if a.Balances[i].Free > empty || a.Balances[i].Locked > empty {
			fltr = append(fltr, a.Balances[i])
		}
	}
	a.Balances = fltr
}
