package binance

import (
	"encoding/json"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// Authentication Struct implements the Authentication interface and takes
// care of authenticating RPC requests for clients with the `BinanceAPIKey` & `BinanceAPISecret` pair
type Authentication struct {
	Pub     string
	Priv    string
	BaseURL string
	Client  http.Client
}

// Client ...
type Client struct {
	Auth *Authentication
}

// Endpoint represent a binance API endpoint
type Endpoint struct {
	Value  string
	Verb   string
	IsAuth bool
	Params map[string]bool
}

// Balance is the binance balance model
type Balance struct {
	Asset  string  `json:"asset"`
	Free   float64 `json:"free"`
	Locked float64 `json:"locked"`
}

// UnmarshalJSON convert json data to Balance model
func (b *Balance) UnmarshalJSON(data []byte) error {
	var err error
	var v map[string]string
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	b.Asset = v["asset"]
	b.Free, err = strconv.ParseFloat(v["free"], 64)
	b.Locked, err = strconv.ParseFloat(v["locked"], 64)
	return err
}

// Account is the reponse to GET /accounts query
type Account struct {
	MakerCommission  int64     `json:"makerCommission"`
	TakerCommission  int64     `json:"takerCommission"`
	BuyerCommission  int64     `json:"buyerCommission"`
	SellerCommission int64     `json:"sellerCommission"`
	CanTrade         bool      `json:"canTrade"`
	CanWithdraw      bool      `json:"canWithdraw"`
	CanDeposit       bool      `json:"canDeposit"`
	UpdateTime       int64     `json:"updateTime"`
	AccountType      string    `json:"accountType"`
	Balances         []Balance `json:"balances"`
	Permissions      []string  `json:"permissions"`
}

// Trade represent a trade history object
type Trade struct {
	Symbol          string  `json:"symbol"`
	ID              int64   `json:"id"`
	OrderID         int64   `json:"orderId"`
	OrderListID     int64   `json:"orderListId"`
	Price           float64 `json:"price"`
	Quantity        float64 `json:"qty"`
	QuoteQuantity   float64 `json:"quoteQty"`
	Commission      float64 `json:"commission"`
	CommissionAsset string  `json:"commissionAsset"`
	Time            int64   `json:"time"`
	IsBuyer         bool    `json:"isBuyer"`
	IsMaker         bool    `json:"isMaker"`
	IsBestMatch     bool    `json:"isBestMatch"`
}

// Trades is a slice of Trade objects
type Trades []Trade

// Get is a convenience method to get the balance for an asset
func (a *Account) Get(asset string) *Balance {
	for el := range a.Balances {
		if a.Balances[el].Asset == strings.ToUpper(asset) {
			return &a.Balances[el]
		}
	}
	return nil
}

// func (t *Trades) Iter() func() (*Trade, bool) {
//     i := -1
//     return func() (*Trade, bool) {
//         i++
//         if i == len(*t) {
//             return nil, false
//         }
//         return &t[i], true
//     }
// }
// // Get is a convenience method to get all "type" orders
// func (t *Trades) Get(position string) *Trades {
// 	for el := range t {

// 	}
// }

// UnmarshalJSON convert json data to Trade structure
func (t *Trade) UnmarshalJSON(data []byte) error {
	var err error
	var v map[string]interface{}
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	t.Symbol = v["symbol"].(string)
	t.ID = int64(v["id"].(float64))
	t.OrderID = int64(v["orderId"].(float64))
	t.OrderListID = int64(v["orderListId"].(float64))
	t.Price, err = strconv.ParseFloat(v["price"].(string), 64)
	t.Quantity, err = strconv.ParseFloat(v["qty"].(string), 64)
	t.QuoteQuantity, err = strconv.ParseFloat(v["quoteQty"].(string), 64)
	t.Commission, err = strconv.ParseFloat(v["commission"].(string), 64)
	t.CommissionAsset = v["commissionAsset"].(string)
	t.Time = int64(v["time"].(float64))
	t.IsBuyer = v["isBuyer"].(bool)
	t.IsMaker = v["isMaker"].(bool)
	t.IsBestMatch = v["isBestMatch"].(bool)
	return err
}

// Price represent a ticker
type Price struct {
	Symbol string  `json:"symbol"`
	Price  float64 `json:"price"`
}

// UnmarshalJSON convert json data to Price structure
func (p *Price) UnmarshalJSON(data []byte) error {
	var err error
	var v map[string]interface{}
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	p.Symbol = v["symbol"].(string)
	p.Price, err = strconv.ParseFloat(v["price"].(string), 64)
	return err
}

// Order represent a market order
type Order struct {
	Symbol                   string    `json:"symbol"`
	OrderID                  int64     `json:"orderId"`
	OrderListID              int64     `json:"orderListId"`
	ClientOrderID            string    `json:"clientOrderId"`
	Price                    float64   `json:"price"`
	OrigQuantity             float64   `json:"origQty"`
	ExecutedQuantity         float64   `json:"executedQty"`
	CumulativeQuoteQuantity  float64   `json:"cummulativeQuoteQty"`
	Status                   string    `json:"status"`
	TimeInForce              string    `json:"timeInForce"`
	Type                     string    `json:"type"`
	Side                     string    `json:"side"`
	StopPrice                float64   `json:"stopPrice"`
	IcebergQuantity          float64   `json:"icebergQty"`
	Time                     time.Time `json:"time"`
	UpdateTime               time.Time `json:"updateTime"`
	IsWorking                bool      `json:"isWorking"`
	OriginQuoteOrderQuantity float64   `json:"origQuoteOrderQty"`
}

// UnmarshalJSON convert json data to Order structure
func (o *Order) UnmarshalJSON(data []byte) error {
	var err error
	var v map[string]interface{}
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	o.Symbol = v["symbol"].(string)
	o.OrderID = int64(v["orderId"].(float64))
	o.OrderListID = int64(v["orderListId"].(float64))
	o.ClientOrderID = v["clientOrderId"].(string)
	o.Price, err = strconv.ParseFloat(v["price"].(string), 64)
	o.OrigQuantity, err = strconv.ParseFloat(v["origQty"].(string), 64)
	o.ExecutedQuantity, err = strconv.ParseFloat(v["executedQty"].(string), 64)
	o.CumulativeQuoteQuantity, err = strconv.ParseFloat(v["cummulativeQuoteQty"].(string), 64)
	o.Status = v["status"].(string)
	o.TimeInForce = v["timeInForce"].(string)
	o.Type = v["type"].(string)
	o.Side = v["side"].(string)
	o.StopPrice, err = strconv.ParseFloat(v["stopPrice"].(string), 64)
	o.IcebergQuantity, err = strconv.ParseFloat(v["icebergQty"].(string), 64)
	o.Time = time.Unix(int64(v["time"].(float64))/1000, 0)
	o.UpdateTime = time.Unix(int64(v["updateTime"].(float64))/1000, 0)
	o.IsWorking = v["isWorking"].(bool)
	o.OriginQuoteOrderQuantity, err = strconv.ParseFloat(v["origQuoteOrderQty"].(string), 64)
	return err
}

// Network represent network info
type Network struct {
	AddressRegex       regexp.Regexp  `json:"addressRegex"`
	Coin               string         `json:"coin"`
	DepositDesc        string         `json:"depositDesc"`
	DepositEnable      bool           `json:"depositEnable"`
	IsDefault          bool           `json:"isDefault"`
	MemoRegex          *regexp.Regexp `json:"memoRegex,omitempty"`
	MinConfirm         int64          `json:"minConfirm"`
	Name               string         `json:"name"`
	Network            string         `json:"network"`
	ResetAddressStatus bool           `json:"resetAddressStatus"`
	SpecialTips        string         `json:"specialTips"`
	UnlockConfirm      int64          `json:"unlockConfirm"`
	WithdrawDesc       string         `json:"withdrawDesc"`
	WithdrawEnable     bool           `json:"withdrawEnable"`
	WithdrawFee        float64        `json:"withdrawFee"`
	WithdrawMin        float64        `json:"withdrawMin"`
}

// Wallet represent Wallet info
type Wallet struct {
	Coin              string    `json:"coin"`
	DepositAllEnable  bool      `json:"depositAllEnable"`
	Free              float64   `json:"free"`
	Freeze            float64   `json:"freeze"`
	IPOAble           float64   `json:"ipoable"`
	IPOIng            float64   `json:"ipoing"`
	IsLegalMoney      bool      `json:"isLegalMoney"`
	Locked            float64   `json:"locked"`
	Name              string    `json:"name"`
	NetworkList       []Network `json:"networkList"`
	Storage           float64   `json:"storage"`
	Trading           bool      `json:"trading"`
	WithdrawAllEnable bool      `json:"withdrawAllEnable"`
	Withdrawing       float64   `json:"withdrawing"`
}
