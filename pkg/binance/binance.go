package binance

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/google/go-querystring/query"
)

// OrderType represent the trade orders types
type OrderType string

// OrderSide represent a new order syde
type OrderSide string

const (
	// OrderSideBuy is ..
	OrderSideBuy = OrderSide("BUY")
	// OrderSideSell is ...
	OrderSideSell = OrderSide("SELL")
	// Limit Order is ...
	Limit = OrderType("LIMIT")
	// Market Order is ...
	Market = OrderType("MARKET")
	// StopLoss is ...
	StopLoss = OrderType("STOP_LOSS")
	// StopLossLimit is ...
	StopLossLimit = OrderType("STOP_LOSS_LIMIT")
	// TakeProfit is ...
	TakeProfit = OrderType("TAKE_PROFIT")
	// TakeProfitLimit is ...
	TakeProfitLimit = OrderType("TAKE_PROFIT_LIMIT")
	// LimitMaker is ...
	LimitMaker = OrderType("LIMIT_MAKER")
	// TimeInforceGoodTillCanceled ...
	TimeInforceGoodTillCanceled = "GTC"

	// BaseURL is the API base endpoint for Binance API
	BaseURL = "https://api.binance.com"
	// APIKeyHeaderKey is the way the secret api is passed for each request
	APIKeyHeaderKey = "X-MBX-APIKEY"
)

// NOrder is a new stop loss order
type NOrder struct {
	Symbol      string    `json:"symbol" url:"symbol"`
	Side        OrderSide `json:"side" url:"side"`
	Type        OrderType `json:"type" url:"type"`
	TimeInForce string    `json:"timeInForce" url:"timeInForce,omitempty"`
	Quantity    float64   `json:"quantity" url:"quantity"`
	Price       float64   `json:"price" url:"price,omitempty"`
	StopPrice   float64   `json:"stopPrice" url:"stopPrice,omitempty"`
}

// ToMap return a map from NOrder struct
func (s *NOrder) ToMap() map[string]interface{} {
	var err error
	var bod []byte
	var foo map[string]interface{}
	if bod, err = json.Marshal(s); err != nil {
		log.Fatal(err)
	}
	if err = json.Unmarshal(bod, &foo); err != nil {
		log.Fatal(err)
	}
	return foo
}

// NewStopLossSell preprare a sell stop loss object
func (s *NOrder) NewStopLossSell(base, quote string, qtt, price float64) *NOrder {
	norder := NOrder{
		Symbol:      base + quote,
		Side:        OrderSideSell,
		Type:        StopLoss,
		TimeInForce: "GTC",
		Quantity:    qtt,
		StopPrice:   price,
	}
	return &norder
}

// Account ...
func (c Client) Account() (*Account, error) {
	var err error
	var acc Account
	var res *http.Response

	if res, err = c.Do(AccountInfo, nil); err != nil {
		return nil, err
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(res.Body)
	str := buf.String()
	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to get account : %s", str)
	}
	if err = json.Unmarshal([]byte(str), &acc); err != nil {
		return nil, err
	}
	return &acc, nil
}

// Trades ...
func (c Client) Trades(base, quote string) ([]Trade, error) {
	var err error
	var trds []Trade
	var res *http.Response
	query := "&symbol=" + strings.ToUpper(base) + strings.ToUpper(quote)
	if res, err = c.Do(TradesInfo, &query); err != nil {
		return nil, err
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(res.Body)
	str := buf.String()
	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to get trades : %s", str)
	}
	if err = json.Unmarshal([]byte(str), &trds); err != nil {
		return nil, err
	}
	return trds, nil
}

// Orders ...
func (c Client) Orders(base, quote string) ([]Order, error) {
	var err error
	var ords []Order
	var res *http.Response
	query := "&symbol=" + strings.ToUpper(base) + strings.ToUpper(quote)
	if res, err = c.Do(OrdersInfo, &query); err != nil {
		return nil, err
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(res.Body)
	str := buf.String()
	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to get orders : %s", str)
	}
	if err = json.Unmarshal([]byte(str), &ords); err != nil {
		return nil, err
	}
	return ords, nil
}

// NewOrder create a new order on binance exchange
func (c Client) NewOrder(o NOrder) (*string, error) {
	var err error
	var res *http.Response
	v, _ := query.Values(o)
	yoo := "&" + v.Encode()
	if res, err = c.Do(NewOrder, &yoo); err != nil {
		return nil, err
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(res.Body)
	str := buf.String()
	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to get orders : %s", str)
	}
	// if err = json.Unmarshal([]byte(str), &respBody); err != nil {
	// 	return nil, err
	// }
	return &str, nil
}

// Price ...
func (c Client) Price(base, quote string) (*Price, error) {
	var err error
	var price Price
	var res *http.Response
	query := "&symbol=" + strings.ToUpper(base) + strings.ToUpper(quote)
	if res, err = c.Do(PriceInfo, &query); err != nil {
		return nil, err
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(res.Body)
	str := buf.String()
	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to get price : %s", str)
	}
	if err = json.Unmarshal([]byte(str), &price); err != nil {
		return nil, err
	}
	return &price, err
}

// Wallet ...
func (c Client) Wallet() (*Wallet, error) {
	var err error
	var wallet Wallet
	var res *http.Response
	if res, err = c.Do(WalletInfo, nil); err != nil {
		return nil, err
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(res.Body)
	str := buf.String()
	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to get wallet info : %s", str)
	}
	if err = json.Unmarshal([]byte(str), &wallet); err != nil {
		return nil, err
	}
	return &wallet, err
}
