package binance

import "net/http"

var (
	// ExchangeInfo return binance platform info
	ExchangeInfo = Endpoint{
		Value:  "/api/v3/exchangeInfo",
		Verb:   http.MethodGet,
		IsAuth: false,
		Params: nil,
	}
	// AccountInfo is the get user account endpoint
	AccountInfo = Endpoint{
		Value:  "/api/v3/account",
		Verb:   http.MethodGet,
		IsAuth: true,
		Params: map[string]bool{
			"timestamp": true,
		},
	}
	// WalletInfo is a better coins balances info object
	WalletInfo = Endpoint{
		Value:  "/sapi/v1/capital/config/getall",
		Verb:   http.MethodGet,
		IsAuth: true,
		Params: map[string]bool{
			"timestamp": true,
		},
	}
	// TradesInfo return trades for a specific account and symbol.
	TradesInfo = Endpoint{
		Value:  "/api/v3/myTrades",
		Verb:   http.MethodGet,
		IsAuth: true,
		Params: map[string]bool{
			"timestamp": true,
			"symbol":    true,
		},
	}
	// OrdersInfo get all account order
	OrdersInfo = Endpoint{
		Value:  "/api/v3/allOrders",
		Verb:   http.MethodGet,
		IsAuth: true,
		Params: map[string]bool{
			"symbol": true,
		},
	}
	// PriceInfo return the latest price for a symbol
	PriceInfo = Endpoint{
		Value:  "/api/v3/ticker/price",
		Verb:   http.MethodGet,
		IsAuth: false,
		Params: map[string]bool{
			"symbol": false,
		},
	}
	// NewOrder create a new market order
	NewOrder = Endpoint{
		Value:  "/api/v3/order",
		Verb:   http.MethodPost,
		IsAuth: true,
		Params: map[string]bool{
			"symbol":      true,
			"side":        true,
			"type":        true,
			"timestamp":   true,
			"stopPrice":   false,
			"quantity":    false,
			"price":       false,
			"timeInForce": false,
		},
	}
	// TestNewOrder create a new market order
	TestNewOrder = Endpoint{
		Value:  "/api/v3/order/test",
		Verb:   http.MethodPost,
		IsAuth: true,
		Params: map[string]bool{
			"symbol":      true,
			"side":        true,
			"type":        true,
			"timestamp":   true,
			"stopPrice":   false,
			"quantity":    false,
			"price":       false,
			"timeInForce": false,
		},
	}
	// CancelOrder cancel a market order
	CancelOrder = Endpoint{
		Value:  "/api/v3/order",
		Verb:   http.MethodDelete,
		IsAuth: true,
		Params: map[string]bool{
			"symbol":            true,
			"orderId":           false,
			"origClientOrderId": false,
			"timestamp":         true,
		},
	}
	// CancelAllOrder cancel all open orders
	CancelAllOrder = Endpoint{
		Value:  "/api/v3/openOrders",
		Verb:   http.MethodDelete,
		IsAuth: true,
		Params: map[string]bool{
			"symbol":    true,
			"timestamp": true,
		},
	}
	// CurrentOpenOrder list all open orders for a symbol
	CurrentOpenOrder = Endpoint{
		Value:  "/api/v3/openOrders",
		Verb:   http.MethodGet,
		IsAuth: true,
		Params: map[string]bool{
			"symbol":    true,
			"timestamp": true,
		},
	}
)
