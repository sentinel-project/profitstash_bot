package etherscan

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
)

const (
	// URL is the etherscan api url
	URL = "https://api.etherscan.io/api"
	// RLCTokenContract is the contract for IExec token
	RLCTokenContract = "0x607F4C5BB672230e8672085532f7e901544a7375"
)

var (
	// EtherscanAPIKey is the api key loaded through env var
	EtherscanAPIKey = os.Getenv("ETHERSCANPRIV")
)

// Response is the etherscan api base response model
type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Result  string `json:"result"`
}

// Client is an etherscan client
type Client struct {
	// PrivKey is the api auth token
	PrivKey string
}

// New new
func New(privkey *string) *Client {
	if EtherscanAPIKey == "" && privkey != nil {
		EtherscanAPIKey = *privkey
	}
	return &Client{
		PrivKey: EtherscanAPIKey,
	}
}

// GetAddressTokens return RLC token found at ethadress
func (c Client) GetAddressTokens(ethaddress string) (float64, error) {
	var err error
	var res *http.Response
	var resval float64
	var bal Response
	query := fmt.Sprintf("?module=account&action=tokenbalance&address=%s&contractaddress=%s&tag=latest&apikey=%s", ethaddress, RLCTokenContract, c.PrivKey)
	if res, err = http.Get(URL + query); err != nil {
		return 0, err
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(res.Body)
	str := buf.String()
	if res.StatusCode != http.StatusOK {
		return resval, fmt.Errorf("failed to get balance : %s", str)
	}
	if err = json.Unmarshal([]byte(str), &bal); err != nil {
		return resval, err
	}
	if resval, err = strconv.ParseFloat(bal.Result, 64); err != nil {
		return resval, err
	}
	resval = resval / 1000000000
	return resval, nil
}

// GetAddressBalance return the amount in ETH of an address
func (c Client) GetAddressBalance(ethaddress string) (float64, error) {
	var err error
	var resval float64
	var bal Response
	query := fmt.Sprintf(URL+"?module=account&action=balance&address=%s&tag=latest&apikey=%s", ethaddress, c.PrivKey)
	res, err := http.Get(query)
	if err != nil {
		return 0, err
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(res.Body)
	str := buf.String()
	if res.StatusCode != http.StatusOK {
		return resval, fmt.Errorf("failed to get balance : %s", str)
	}
	if err = json.Unmarshal([]byte(str), &bal); err != nil {
		return resval, err
	}
	if resval, err = strconv.ParseFloat(bal.Result, 64); err != nil {
		return resval, err
	}
	resval = resval / 1000000000000000000
	return resval, nil
}
