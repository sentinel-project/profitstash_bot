package repository

import (
	"balance_api/pkg/models"
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"

	// Import postgres connector
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// DBConfig ...
type DBConfig struct {
	DB   *gorm.DB
	Host string
	Port string
	User string
	Pass string
	Name string
}

// Init ...
func (c *DBConfig) Init() {
	var err error
	var db *gorm.DB
	var coStr string

	if os.Getenv("STAGE") == "" || os.Getenv("STAGE") == "dev" {
		coStr = "host=localhost port=5432 user=postgres password=password dbname=stash sslmode=disable"
	} else {
		coStr = fmt.Sprintf(
			"host=%s port=%s user=%s password=%s dbname=%s",
			os.Getenv("DB_HOST"),
			os.Getenv("DB_PORT"),
			os.Getenv("DB_USER"),
			os.Getenv("DB_PASS"),
			os.Getenv("DB_NAME"),
		)
	}
	if db, err = gorm.Open(
		"postgres",
		coStr,
	); err != nil {
		log.Fatal(err)
	} else {
		fmt.Printf("Successfully connected to db [%s]\n", coStr)
	}
	defer db.Close()
	c.DB = db
	models.Migrate(db)
}

// GetDB ...
func (c DBConfig) GetDB() *gorm.DB {
	if c.DB == nil {
		c.Init()
	}
	return c.DB
}
