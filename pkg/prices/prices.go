package prices

import (
	"balance_api/pkg/config"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
)

const (
	// BaseURL is the cryptocompare api base url
	BaseURL = "https://min-api.cryptocompare.com"
)

var (
	defConfFile = "conf.toml"
	// CryptoCompareAPIKey ...
	CryptoCompareAPIKey = os.Getenv("CRYPTOCOMPAREPRIV")
	// DefaultPriceFeed is the exchange on which prices are fetched by default
	DefaultPriceFeed = "binance"
)

// Client ...
type Client struct {
	APIKey    string
	BaseURL   string
	PriceFeed string
}

// New return a new cryptocompare price client
func New(apiKey, pricefeed, conffile *string) *Client {
	var dev = os.Getenv("STAGE") == "" || os.Getenv("STAGE") == "dev"
	if dev || conffile != nil {
		if conffile == nil {
			conffile = &defConfFile
		}
		accs := config.FromFile(*conffile)
		ccacc := accs.AccountByName("cryptocompare")
		CryptoCompareAPIKey = ccacc.Priv
	}
	if CryptoCompareAPIKey == "" && apiKey != nil {
		CryptoCompareAPIKey = *apiKey
	}
	if pricefeed != nil {
		DefaultPriceFeed = *pricefeed
	}
	return &Client{
		APIKey:    CryptoCompareAPIKey,
		BaseURL:   BaseURL,
		PriceFeed: DefaultPriceFeed,
	}
}

// GetPrice return spot price for a currency pair
func (c *Client) GetPrice(base, quote string) (price float64, err error) {
	var nreq *http.Request
	var res *http.Response
	var resBody map[string]float64
	var endpoint = fmt.Sprintf("/data/price?fsym=%s&tsyms=%s&e=%s", base, quote, c.PriceFeed)

	if nreq, err = http.NewRequest(http.MethodGet, c.BaseURL+endpoint, nil); err != nil {
		return price, err
	}
	nreq.Header.Set("Authorization", c.APIKey)
	res, err = http.DefaultClient.Do(nreq)
	if res.StatusCode != http.StatusOK {
		return price, fmt.Errorf("failed to request CryptoCompare API, got status %v", res.StatusCode)
	}
	defer res.Body.Close()
	buf := new(bytes.Buffer)
	buf.ReadFrom(res.Body)
	str := buf.String()
	if err = json.Unmarshal([]byte(str), &resBody); err != nil {
		return price, err
	}
	for key, val := range resBody {
		if key == quote {
			return val, nil
		}
	}
	return price, fmt.Errorf("failed to get pair price")
}
