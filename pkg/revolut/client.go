package revolut

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"os"
)

const (
	// BaseURL is the revolut api base URl
	BaseURL = "https://api.revolut.com"
	// DefaultAuthUser is the default basic token username
	DefaultAuthUser = "App"
	// DefaultAuthPass is the default basic token password
	DefaultAuthPass = "S9WUnSFBy67gWan7"
	// DefaultAuth is a base auth header value
	DefaultAuth = "Basic QXBwOlM5V1VuU0ZCeTY3Z1dhbjc="
)

var (
	// REVOPhoneNumber is the default env var to store user phone (ID)
	REVOPhoneNumber = os.Getenv("REVOPUB")
	// REVOPassword is the default env var to store user password
	REVOPassword = os.Getenv("REVOPRIV")
	// REVODeviceUUID is an identifier for the device this programm is running on
	REVODeviceUUID = os.Getenv("REVOBOTID")
)

// Token is the auth token
type Token struct {
	AccessToken string `json:"accessToken"`
}

// Authentication is the needed credentials for revolut api authentication
type Authentication struct {
	PhoneNumber string
	AppPassword string
	DeviceUUID  string
	Client      http.Client
	Token
}

// Client ...
type Client struct {
	Auth *Authentication
}

// CreateMultipartFormData is needed for selfie biometric authentication
func CreateMultipartFormData(fieldName, fileName string, file io.Reader) (r bytes.Buffer, err error) {
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	var fw io.Writer
	if fw, err = w.CreateFormFile(fieldName, fileName); err != nil {
		return b, err
	}
	if _, err = io.Copy(fw, file); err != nil {
		return b, err
	}
	w.Close()
	return b, err
}
