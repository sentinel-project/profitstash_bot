package revolut

import "net/http"

const (
	// SpoofUserAgent is here to mimic the android application UA
	SpoofUserAgent = "Revolut/5.5 500500250 (CLI; Android 4.4.2)"
	// SpoofClientVersion is also here to mimic android app headers
	SpoofClientVersion = "6.34.3"
	// ContentTypeURLEncoded is mime for url encoded params
	ContentTypeURLEncoded = "application/x-www-form-urlencoded"
	// ContentTypeJSON is mime for json params
	ContentTypeJSON = "application/json"
	// ContentTypeFormData is mime for form datas
	ContentTypeFormData = "multipart/form-data"
)

// Endpoint is a revolut endpoint
type Endpoint struct {
	Value       string
	Verb        string
	ContentType string
	IsAuth      bool
	Holder      interface{}
	Params      map[string]bool
}

var (
	// InitToken launch an auth request (getting mfaToken)
	InitToken = Endpoint{
		Value:       "/signin",
		Verb:        http.MethodPost,
		ContentType: ContentTypeJSON,
		Holder: struct {
			Channel string `json:"channel"`
		}{},
		Params: map[string]bool{
			"phone":    true,
			"password": true,
		},
	}
	// GetToken request an authentication token after getting the 2FA code
	GetToken = Endpoint{
		Value:       "/signin/confirm",
		Verb:        http.MethodPost,
		ContentType: ContentTypeJSON,
		Holder: struct {
			ThirdFactorAuthAccessToken string `json:"thirdFactorAuthAccessToken"`
		}{},
		Params: map[string]bool{
			"phone": true,
			"code":  true,
		},
	}
	// BiometricLogin send a selfie to validate the third factor auth
	BiometricLogin = Endpoint{
		Value:       "/biometric-signin/selfie",
		Verb:        http.MethodPost,
		ContentType: ContentTypeFormData,
	}
	// GetAccounts get the user wallet balances
	GetAccounts = Endpoint{
		Value:       "/user/current/wallet",
		Verb:        http.MethodGet,
		ContentType: ContentTypeJSON,
		Params:      nil,
	}
)
