package revolut

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/google/uuid"
)

// New return a new client against a phone number and password pairs
func New(phonenumber *string, password *string, deviceuuid *string) *Client {
	if REVOPhoneNumber == "" && phonenumber != nil {
		REVOPhoneNumber = *phonenumber
	}
	if REVOPassword == "" && password != nil {
		REVOPassword = *password
	}
	if REVODeviceUUID == "" && deviceuuid != nil {
		REVODeviceUUID = *deviceuuid
	} else if REVODeviceUUID == "" {
		REVODeviceUUID = uuid.New().String()
	}
	REVOAccount := &Authentication{
		PhoneNumber: REVOPhoneNumber,
		AppPassword: REVOPassword,
		DeviceUUID:  REVODeviceUUID,
		Client:      *http.DefaultClient,
	}
	nCli := &Client{
		Auth: REVOAccount,
	}
	// if err := nCli.Authenticate(); err != nil {
	// 	fmt.Printf("Error(n26/New): %s\n", err.Error())
	// 	return nil
	// }
	return nCli
}

// Do ...
func (c Client) Do(e Endpoint, params *string, holder interface{}) error {
	var (
		err      error
		reqParam string
		req      *http.Request
		resp     *http.Response
	)
	// Creating request
	if req, err = http.NewRequest(e.Verb, BaseURL+e.Value, nil); err != nil {
		return err
	}
	// Setting header
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", e.ContentType)
	req.Header.Set("X-Device-Id", c.Auth.DeviceUUID)
	req.Header.Set("X-Client-Version", SpoofClientVersion)
	req.Header.Set("User-Agent", SpoofUserAgent)
	if e.IsAuth {
		req.Header.Set("Authorization", "bearer "+c.Auth.AccessToken)
	} else {
		req.Header.Set("Authorization", DefaultAuth)
	}
	// Setting request param if any
	if params != nil && e.ContentType == ContentTypeURLEncoded {
		reqParam = url.QueryEscape(*params)
	}
	if e.Verb != http.MethodGet {
		req.Body = ioutil.NopCloser(strings.NewReader(*params))
	} else {
		req.URL.RawQuery = reqParam
	}
	// Doing request
	if resp, err = c.Auth.Client.Do(req); err != nil {
		return err
	}
	defer resp.Body.Close()
	// Deserialize
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	str := buf.String()
	if err := json.Unmarshal([]byte(str), &holder); err != nil {
		return err
	}
	return nil
}
