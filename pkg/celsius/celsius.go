package celsius

import (
	"balance_api/pkg/fxrates"
	"strconv"
	"time"

	"log"
)

const (
	// CelsiusCSVHeaderID is the header first row id in celsius report csv
	CelsiusCSVHeaderID = "Internal id"
	// CelsiusDateLayout is the date format string for csv report
	CelsiusDateLayout = "January 2 2006 3:04 PM"
)

// CSVBalance is the total balances in crypto and eur value
type CSVBalance struct {
	Sum      float64
	USDValue float64
	EURValue float64
}

// Transaction is a celsius app transaction report
type Transaction struct {
	UID string
	CSVReport
	USDValue  float64
	EURValue  float64
	Confirmed bool
}

// Report is a celsius csv report
type Report struct {
	FName        string
	Transactions []Transaction
}

var converter = fxrates.APIData{}

// NewCSV turn a csv file into a Report slice object
func NewCSV(fname string) (*Report, error) {
	var err error
	var lines [][]string
	var celrecords = Report{
		FName: fname,
	}

	if lines, err = TokenizeCSV(fname, CelsiusCSVHeaderID); err != nil {
		return nil, err
	}
	for _, line := range lines {
		var nrecord Transaction
		nrecord = nrecord.Serialize(line).ToEUR()
		celrecords.Transactions = append(celrecords.Transactions, nrecord)
	}
	return &celrecords, err
}

// Serialize from a csv row
func (c Transaction) Serialize(tokenized []string) Transaction {
	var err error
	c.UID = tokenized[0]
	if c.Timestamp, err = time.Parse(CelsiusDateLayout, tokenized[1]+tokenized[2]); err != nil {
		log.Fatal(err)
	}
	c.TransactionType = tokenized[3]
	c.Asset = tokenized[4]
	if c.QuantityTransacted, err = strconv.ParseFloat(tokenized[5], 64); err != nil {
		log.Fatal(err)
	}
	if c.USDValue, err = strconv.ParseFloat(tokenized[6], 64); err != nil {
		log.Fatal(err)
	}

	if tokenized[7] == "Yes" {
		c.Confirmed = true
	} else if tokenized[7] == "No" {
		c.Confirmed = false
	}
	return c
}

// ToEUR convert USD datas to EUR
func (c Transaction) ToEUR() Transaction {
	c.EURValue = converter.ToEUR(c.USDValue, "USD")
	return c
}

// GetBalances take a celsius slice and return a map with asset name index and asset balance value
func (r *Report) GetBalances() map[string]CSVBalance {
	var balances = map[string]CSVBalance{}
	for _, val := range r.Transactions {
		balances[val.Asset] = CSVBalance{
			Sum:      balances[val.Asset].Sum + val.QuantityTransacted,
			USDValue: balances[val.Asset].USDValue + val.USDValue,
			EURValue: balances[val.Asset].EURValue + val.EURValue,
		}
	}
	return balances
}
