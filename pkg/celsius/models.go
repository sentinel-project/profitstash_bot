package celsius

import (
	"balance_api/pkg/prices"
	"reflect"
	"strconv"
	"strings"
)

// Endpoint represent a celsius API endpoint
type Endpoint struct {
	Value  string
	Verb   string
	IsAuth bool
	Params map[string]bool
}

// Balance is ...
type Balance struct {
	Asset    string
	Value    float64
	BTCValue *float64
}

// Balances is a better format for wallet balances
type Balances struct {
	Datas  []Balance
	BTCSum *float64
}

// BalanceResponse is the wallet balances model
type BalanceResponse struct {
	Balance struct {
		Eth       string `json:"eth"`
		Btc       string `json:"btc"`
		Dash      string `json:"dash"`
		Bch       string `json:"bch"`
		Ltc       string `json:"ltc"`
		Zec       string `json:"zec"`
		Btg       string `json:"btg"`
		Xrp       string `json:"xrp"`
		Xlm       string `json:"xlm"`
		Omg       string `json:"omg"`
		Tusd      string `json:"tusd"`
		Gusd      string `json:"gusd"`
		Pax       string `json:"pax"`
		Usdc      string `json:"usdc"`
		Dai       string `json:"dai"`
		Mcdai     string `json:"mcdai"`
		Cel       string `json:"cel"`
		Zrx       string `json:"zrx"`
		Orbs      string `json:"orbs"`
		UsdtErc20 string `json:"usdt erc20"`
		Tgbp      string `json:"tgbp"`
		Taud      string `json:"taud"`
		Thkd      string `json:"thkd"`
		Tcad      string `json:"tcad"`
		Eos       string `json:"eos"`
		Sga       string `json:"sga"`
		Xaut      string `json:"xaut"`
	} `json:"balance"`
}

// ToBalances format a balance response to more convenient Balances model
func (b *BalanceResponse) ToBalances() *Balances {
	var nBalances Balances
	v := reflect.ValueOf(b.Balance)
	typeOfS := v.Type()
	for i := 0; i < v.NumField(); i++ {
		val, _ := strconv.ParseFloat(v.Field(i).String(), 64)
		nBalances.Datas = append(nBalances.Datas, Balance{
			Asset: strings.ToUpper(typeOfS.Field(i).Name),
			Value: val,
		})
	}
	return &nBalances
}

// FilterEmpty remove empty balance from balanceResponse
func (b *Balances) FilterEmpty() *Balances {
	var nbals []Balance
	for _, bal := range b.Datas {
		if bal.Value > 0 && (bal.BTCValue != nil && *bal.BTCValue > 0.0001) {
			nbals = append(nbals, bal)
		}
	}
	b.Datas = nbals
	return b
}

// GetBTCVal query price feed to get bitcoin value of an asset
func (b *Balance) GetBTCVal(prInfo *prices.Client) *Balance {
	var nop float64 = 0
	if b.Asset == "BTC" {
		b.BTCValue = &b.Value
		return b
	} else if b.Value > 0 {
		v, _ := prInfo.GetPrice(b.Asset, "BTC")
		pr := v * b.Value
		b.BTCValue = &pr
	} else {
		b.BTCValue = &nop
	}
	return b
}

// GetBTCVal return balances total value in btc
func (b *Balances) GetBTCVal(prInfo *prices.Client) *Balances {
	var sum float64
	for i := range b.Datas {
		if b.Datas[i].BTCValue != nil {
			sum += *b.Datas[i].BTCValue
		} else {
			b.Datas[i].GetBTCVal(prInfo)
			if b.Datas[i].BTCValue != nil {
				sum += *b.Datas[i].BTCValue
			}
		}
	}
	b.BTCSum = &sum
	return b
}
