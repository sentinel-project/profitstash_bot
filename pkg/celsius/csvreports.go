package celsius

import (
	"bufio"
	"os"
	"strings"
	"time"
)

// CSVReport is the generic informations present in each csv report
type CSVReport struct {
	Timestamp          time.Time
	TransactionType    string
	Asset              string
	QuantityTransacted float64
}

// TokenizeCSV ...
func TokenizeCSV(fname string, headerID string) ([][]string, error) {
	var err error
	var records [][]string
	var file *os.File

	if file, err = os.Open(fname); err != nil {
		return nil, err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "\"") {
			line = strings.ReplaceAll(line, "\"", "")
		}
		tokenized := strings.Split(line, ",")
		if tokenized[0] == headerID { // Skip headers row
			continue
		}
		records = append(records, tokenized)
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return records, err
}
