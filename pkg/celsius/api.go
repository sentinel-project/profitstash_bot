package celsius

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

const (
	// BaseURL is the celsius api base URL
	BaseURL = "https://wallet-api.celsius.network"
	// HeaderKeyPartnerToken ...
	HeaderKeyPartnerToken = "X-Cel-Partner-Token"
	// HeaderKeyAPIKey ...
	HeaderKeyAPIKey = "X-Cel-Api-Key"
)

var (
	// CelsiusPartnerToken is the...
	CelsiusPartnerToken = os.Getenv("CELSIUSPUB")
	// CelsiusAPIKey is the ...
	CelsiusAPIKey = os.Getenv("CELSIUSPRIV")
	// BalancesEndpoint ...
	BalancesEndpoint = Endpoint{
		Value:  "/wallet/balance",
		Verb:   http.MethodGet,
		IsAuth: true,
		Params: nil,
	}
)

// Client store api auth creds
type Client struct {
	PartnerToken string
	APIKey       string
	BaseURL      string
	Client       *http.Client
}

// New return a client to query celsius api
func New(partnertoken, apikey *string) *Client {
	if CelsiusPartnerToken == "" && partnertoken != nil {
		CelsiusPartnerToken = *partnertoken
	}
	if CelsiusAPIKey == "" && apikey != nil {
		CelsiusAPIKey = *apikey
	}
	return &Client{
		PartnerToken: CelsiusPartnerToken,
		APIKey:       CelsiusAPIKey,
		BaseURL:      BaseURL,
		Client:       http.DefaultClient,
	}
}

// Do is a wrapper around http.Client.Do method (add auth when needed)
func (c Client) Do(e Endpoint, params *string, holder interface{}) error {
	var (
		err     error
		res     *http.Response
		req     *http.Request
		reqBody string
	)
	if req, err = http.NewRequest(e.Verb, c.BaseURL+e.Value, nil); err != nil {
		return err
	}
	if e.IsAuth {
		req.Header.Set(HeaderKeyPartnerToken, c.PartnerToken)
		req.Header.Set(HeaderKeyAPIKey, c.APIKey)
	}
	if e.Params != nil && params != nil {
		reqBody = *params
	}
	if e.Verb != http.MethodGet {
		req.Body = ioutil.NopCloser(strings.NewReader(reqBody))
	} else {
		req.URL.RawQuery = reqBody
	}
	if res, err = c.Client.Do(req); err != nil {
		return err
	}
	defer res.Body.Close()
	buf := new(bytes.Buffer)
	buf.ReadFrom(res.Body)
	str := buf.String()
	if err = json.Unmarshal([]byte(str), &holder); err != nil {
		return err
	}
	return err
}

// GetBalances query celsius api to get user balances values
func (c *Client) GetBalances() (bals *Balances, err error) {
	var balresp BalanceResponse
	if err = c.Do(BalancesEndpoint, nil, &balresp); err != nil {
		return nil, err
	}
	bals = balresp.ToBalances()
	return bals, err
}
